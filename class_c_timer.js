var class_c_timer =
[
    [ "CTimer", "class_c_timer.html#a93e4d770fd09f5e1b984768620cc52bf", null ],
    [ "getElapsedTime", "class_c_timer.html#ac3d84295f07cd87912a3b7d42677371e", null ],
    [ "isPaused", "class_c_timer.html#ac8a39ced7468b8262d19e4cdc00b947e", null ],
    [ "isStarted", "class_c_timer.html#a6825afddd80ea0fbe0e080fc11458fc2", null ],
    [ "pause", "class_c_timer.html#a18446b0260ea279048d9f10fbe8acebc", null ],
    [ "restart", "class_c_timer.html#a7026ba45b564e103008cb9052bc07c6a", null ],
    [ "start", "class_c_timer.html#a04aa066aa2daef2f6887947656806aa8", null ],
    [ "stop", "class_c_timer.html#a6cfccc69b2709f8db6835f1cdf586d64", null ]
];