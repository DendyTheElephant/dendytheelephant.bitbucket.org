var _s_core_engine_8h =
[
    [ "SCoreEngine", "class_s_core_engine.html", "class_s_core_engine" ],
    [ "MESHES_EXTENSION", "_s_core_engine_8h.html#ab76fb8ba93083c0f8d25e5c4215b5b87", null ],
    [ "MESHES_FOLDER", "_s_core_engine_8h.html#af0724596faf6dd8448ea73d1a944e156", null ],
    [ "SHADER_FRAGMENT_EXTENSION", "_s_core_engine_8h.html#a2980fc559d1044eb893705e12df8cb69", null ],
    [ "SHADER_VERTEX_EXTENSION", "_s_core_engine_8h.html#afaa1ea29a2081bff56515bc773565919", null ],
    [ "SHADERS_FOLDER", "_s_core_engine_8h.html#a188118b33111b6fe43e65cf34b1e2c18", null ],
    [ "WINDOW_CAPTION", "_s_core_engine_8h.html#ada956bd1f139d644f131a27c7a484078", null ],
    [ "WINDOW_HEIGHT", "_s_core_engine_8h.html#a5473cf64fa979b48335079c99532e243", null ],
    [ "WINDOW_WIDTH", "_s_core_engine_8h.html#a498d9f026138406895e9a34b504ac6a6", null ]
];