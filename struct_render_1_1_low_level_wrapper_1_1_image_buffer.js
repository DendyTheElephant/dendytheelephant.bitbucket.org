var struct_render_1_1_low_level_wrapper_1_1_image_buffer =
[
    [ "ImageBuffer", "struct_render_1_1_low_level_wrapper_1_1_image_buffer.html#a4ec4732fee797a68980ee2246228965f", null ],
    [ "free", "struct_render_1_1_low_level_wrapper_1_1_image_buffer.html#a97e967ed4dd863d8c7ebd2f1dc6241e8", null ],
    [ "initialize", "struct_render_1_1_low_level_wrapper_1_1_image_buffer.html#a5b27e51c4ab797a1835be059c4f89a09", null ],
    [ "id", "struct_render_1_1_low_level_wrapper_1_1_image_buffer.html#aa80751e306136b688c65650875660574", null ],
    [ "texture", "struct_render_1_1_low_level_wrapper_1_1_image_buffer.html#adb265560e8967a5b5d8339e153286365", null ],
    [ "VAO_id", "struct_render_1_1_low_level_wrapper_1_1_image_buffer.html#aa2b457fff3f2bf7dc83fc503cbdfce11", null ],
    [ "VBO_Quad", "struct_render_1_1_low_level_wrapper_1_1_image_buffer.html#ac231ffe2cf0accdcb974c20b7841dd31", null ]
];