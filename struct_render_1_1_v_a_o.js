var struct_render_1_1_v_a_o =
[
    [ "VAO", "struct_render_1_1_v_a_o.html#af0c59b51d937991a64263e9f7fa0f881", null ],
    [ "free", "struct_render_1_1_v_a_o.html#a206ef3b00b5654564df3d61216870a50", null ],
    [ "loadToGPU", "struct_render_1_1_v_a_o.html#afbe4512c56b6e65b2e587ed00576c712", null ],
    [ "id", "struct_render_1_1_v_a_o.html#afc50a74bf4a3c438673de5cb6adb93b1", null ],
    [ "VBO_Colors", "struct_render_1_1_v_a_o.html#a14c8624629df0140a53afc60f4520d6c", null ],
    [ "VBO_Indices", "struct_render_1_1_v_a_o.html#ae09272b88a1908002e40e405fe2a28a4", null ],
    [ "VBO_Normals", "struct_render_1_1_v_a_o.html#a070f094a75d934b454e4c8c919fc97c9", null ],
    [ "VBO_Vertices", "struct_render_1_1_v_a_o.html#a900ed81689807e98a9de42c904ad8fb3", null ]
];