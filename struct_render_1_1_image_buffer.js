var struct_render_1_1_image_buffer =
[
    [ "ImageBuffer", "struct_render_1_1_image_buffer.html#ae079b86576f411129ecf5dfced95ebc3", null ],
    [ "free", "struct_render_1_1_image_buffer.html#acab6a406219c5239d540b7f7f8e92f82", null ],
    [ "initialize", "struct_render_1_1_image_buffer.html#a0b9a717f2db8215561405b6cd267f718", null ],
    [ "id", "struct_render_1_1_image_buffer.html#a47f5f937bf7825fdcd4015a3fee19af7", null ],
    [ "texture", "struct_render_1_1_image_buffer.html#aee9977f1b19294be10824b796ead1c42", null ],
    [ "VAO_id", "struct_render_1_1_image_buffer.html#aca7c5ea5e5d4acece47d101675d50f4c", null ],
    [ "VBO_Quad", "struct_render_1_1_image_buffer.html#a2686e946f9d48ee451d628536a74afb6", null ]
];