var struct_render_1_1_g_buffer =
[
    [ "GBuffer", "struct_render_1_1_g_buffer.html#a0a936848b4f7728aed4e092ed4445c55", null ],
    [ "free", "struct_render_1_1_g_buffer.html#afd477c78cf3c329ecd7b6156f4493464", null ],
    [ "initialize", "struct_render_1_1_g_buffer.html#abe7c5653737ad2ed9e474f210a090a79", null ],
    [ "colorTexture", "struct_render_1_1_g_buffer.html#a8c2e1584753e1546f48075ca3e542bb5", null ],
    [ "depthTexture", "struct_render_1_1_g_buffer.html#a09d1f47074b9024515041fa813bf05c3", null ],
    [ "id", "struct_render_1_1_g_buffer.html#af953e00040a4659ae7d7352b0ca23e6f", null ],
    [ "normalTexture", "struct_render_1_1_g_buffer.html#a677424e2504157f7d7581f5846c5e495", null ],
    [ "typeTexture", "struct_render_1_1_g_buffer.html#a49ff8613babaa597e9104da8816beda2", null ],
    [ "VAO_id", "struct_render_1_1_g_buffer.html#a2b51083537c9bd11c9106fe8cca8222b", null ],
    [ "VBO_Quad", "struct_render_1_1_g_buffer.html#a1355609eac190e5aaccf2923cc8ae6f2", null ]
];