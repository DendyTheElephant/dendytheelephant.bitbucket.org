var structrender_1_1_v_a_o =
[
    [ "VAO", "structrender_1_1_v_a_o.html#a71f7f1c4689ee367a65ab7296a4421d5", null ],
    [ "free", "structrender_1_1_v_a_o.html#a8c3c7fa9802206d08a9a0259b490356b", null ],
    [ "loadToGPU", "structrender_1_1_v_a_o.html#a3b4d375d1bab4451be2a6978f12b451d", null ],
    [ "id", "structrender_1_1_v_a_o.html#ab22eae990c3e08f3c7e7d0d01340faf8", null ],
    [ "VBO_Colors", "structrender_1_1_v_a_o.html#a09b3b357d379838743c2035d30398a4f", null ],
    [ "VBO_Indices", "structrender_1_1_v_a_o.html#a6bdf2b235f4fd292dcc96663c105d397", null ],
    [ "VBO_Normals", "structrender_1_1_v_a_o.html#af066ecf4482fa82139e1e7ff90297fab", null ],
    [ "VBO_Vertices", "structrender_1_1_v_a_o.html#a41b864db7e017ec319c3e245d6400d65", null ]
];