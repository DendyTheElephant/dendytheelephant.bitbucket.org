var structrender_1_1_g_buffer =
[
    [ "GBuffer", "structrender_1_1_g_buffer.html#aa3903c42076a72fa12a6b895c71ba8ef", null ],
    [ "free", "structrender_1_1_g_buffer.html#a73362688a4d979386579630c4bad210d", null ],
    [ "initialize", "structrender_1_1_g_buffer.html#a78c7037b06e32fccf68aed9f0aff2d46", null ],
    [ "colorTexture", "structrender_1_1_g_buffer.html#a2360d86e84c85abfd2d651f2efdb6ee0", null ],
    [ "depthTexture", "structrender_1_1_g_buffer.html#afd3864821db2982a6f74731693b978dd", null ],
    [ "id", "structrender_1_1_g_buffer.html#a5cf059b7a1b488cb83aac14af5400cfd", null ],
    [ "normalTexture", "structrender_1_1_g_buffer.html#aceb674941af99581958a0b83d50f8552", null ],
    [ "typeTexture", "structrender_1_1_g_buffer.html#a52dce2b24bb04fe0e900f3e580018520", null ],
    [ "VAO_id", "structrender_1_1_g_buffer.html#ad88a76d003bb3001a135ab7e90e3ed1e", null ],
    [ "VBO_Quad", "structrender_1_1_g_buffer.html#a6c38cbbcbe0e88af725d91a6eb54567d", null ]
];