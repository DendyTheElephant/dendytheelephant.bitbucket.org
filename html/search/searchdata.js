var indexSectionsWithContent =
{
  0: "bcdfgilmnoprstuvw~",
  1: "cgisv",
  2: "r",
  3: "clmrs",
  4: "cdfgilmoprsuv~",
  5: "cdintv",
  6: "bt",
  7: "msw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enumvalues",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerator",
  7: "Macros"
};

