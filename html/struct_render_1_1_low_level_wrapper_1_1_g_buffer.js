var struct_render_1_1_low_level_wrapper_1_1_g_buffer =
[
    [ "GBuffer", "struct_render_1_1_low_level_wrapper_1_1_g_buffer.html#ab41a839c5395992fe4f4945707ffe964", null ],
    [ "free", "struct_render_1_1_low_level_wrapper_1_1_g_buffer.html#a234a571bceff473b02ad8467bace7499", null ],
    [ "initialize", "struct_render_1_1_low_level_wrapper_1_1_g_buffer.html#ac5100c8b81282427a56b8618d333fb99", null ],
    [ "colorTexture", "struct_render_1_1_low_level_wrapper_1_1_g_buffer.html#aedd59e52f0f2bc1e3188670fa41a4549", null ],
    [ "depthTexture", "struct_render_1_1_low_level_wrapper_1_1_g_buffer.html#ae43189afa9a4b5514be7175321a85ab2", null ],
    [ "id", "struct_render_1_1_low_level_wrapper_1_1_g_buffer.html#a73b756833b81a07d96bbb2c7928349cf", null ],
    [ "normalTexture", "struct_render_1_1_low_level_wrapper_1_1_g_buffer.html#a783de412de35f563a1a2d9627d533dd8", null ],
    [ "typeTexture", "struct_render_1_1_low_level_wrapper_1_1_g_buffer.html#aade11a19af8305cda85c90a46fea97c2", null ],
    [ "VAO_id", "struct_render_1_1_low_level_wrapper_1_1_g_buffer.html#a183d9f3483e625bc08a366966a9804d2", null ],
    [ "VBO_Quad", "struct_render_1_1_low_level_wrapper_1_1_g_buffer.html#afd4d0ed8061d9c841219e0109ffa6d13", null ]
];